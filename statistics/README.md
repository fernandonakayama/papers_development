**R (CRAN)**
R is a free software environment for statistical computing and graphics. It compiles and runs on a wide variety of UNIX platforms, Windows and MacOS. To download R, please choose your preferred CRAN mirror.

[CRAN-R](https://cran.r-project.org "CRAN-R")

I prefer using R with its graphical interface R-studio. R-studio desktop is free in its open source edition.

[R-Studio](https://rstudio.com/products/rstudio/ "R-Studio")

**JASP**
JASP is an open-source project supported by the University of Amsterdam. JASP has an intuitive interface that was designed with the user in mind. JASP offers standard analysis procedures in both their classical and Bayesian form.

[JASP](https://jasp-stats.org/ "JASP")

**Examples:**

<a href="https://ibb.co/hB9wJcx"><img src="https://i.ibb.co/T42dx1D/Screenshot-20201120-094852.png" alt="Screenshot-20201120-094852" border="0"></a>
<a href="https://ibb.co/n8CRYQD"><img src="https://i.ibb.co/93tG1yq/Screenshot-20201120-094831.png" alt="Screenshot-20201120-094831" border="0"></a>
<a href="https://ibb.co/x8MrjJT"><img src="https://i.ibb.co/fGCTp4w/Screenshot-20201120-094721.png" alt="Screenshot-20201120-094721" border="0"></a>
<a href="https://ibb.co/ZJtjY9S"><img src="https://i.ibb.co/n0ZGnpB/Screenshot-20201120-094516.png" alt="Screenshot-20201120-094516" border="0"></a><a href="https://ibb.co/hB9wJcx"><img src="https://i.ibb.co/T42dx1D/Screenshot-20201120-094852.png" alt="Screenshot-20201120-094852" border="0"></a>
<a href="https://ibb.co/n8CRYQD"><img src="https://i.ibb.co/93tG1yq/Screenshot-20201120-094831.png" alt="Screenshot-20201120-094831" border="0"></a>
<a href="https://ibb.co/x8MrjJT"><img src="https://i.ibb.co/fGCTp4w/Screenshot-20201120-094721.png" alt="Screenshot-20201120-094721" border="0"></a>
<a href="https://ibb.co/ZJtjY9S"><img src="https://i.ibb.co/n0ZGnpB/Screenshot-20201120-094516.png" alt="Screenshot-20201120-094516" border="0"></a>