**Multimedia tools**

- **StreamYard**- (Freeware) - Stream content to Youtube
A useful tool to stream videos and meetings to Youtube and other sites.

- Pros: Easy to set up and use

- Cons: 20 hours limit, six users limit per meeting, a watermark (logo), in the free version.

Example created with StreamYard - [Fernando Nakayama Doctoral Defense](https://youtu.be/KnwzsJo8k8A "Fernando Nakayama Doctoral Defense")

- **Kazam** (Linux) - Screen capture tool that captures audio
Simple tool to capture the screen content (with optional audio). Useful for capturing presentations and classes.
[Example created with Kazam ](https://gitlab.com/fernandonakayama/papers_development/-/blob/master/images_graphics/inkscape.mp4 "Example created with Kazam ")

- **Openshot** (Linux) - Video and audio editing

Very useful tool to edit video. You can use an addition audio track, overlap audio, add subtitles, create transition effects, etc.

**Example  created with Openshot:** [WRNP 2019 Presentation](https://youtu.be/REEJ4XmxIAo "WRNP 2019")