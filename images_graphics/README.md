**Gimp - Image editing tool (Open source)**

Q. What do I use it for?

A. Basically to manipulate images in conventional formats such as .jpg .png etc. 

[Gimp](https://www.gimp.org/ "Gimp")

**Inkscape - Vector graphics tool (Open source)***

Q. What do I use it for?

A. I use it whenever I need to manipulate vector images. Depending on the type of paper I' m working I need to deliver images in vector format like .eps or .ps and I rely on Inkscape to build or modify vector images. It is also pretty useful to import .pdf images from Microsoft Power Point and exporting to vector format.

[Inkscape](https://inkscape.org/pt-br/ "Inkscape")

I usually create my images in Power Point for convenience and due to compatibility issues. However, some publishers require a full vector image. I use inkscape to convert the PDF files exported in Microsoft Powerpoint. Some conferences also verify PDF files for fonts compatibility, if that is the case you can also convert fonts into paths in inkscape.  


[VIDEO - Importing PDF files and saving as EPS.](https://gitlab.com/fernandonakayama/papers_development/-/blob/master/images_graphics/inkscape.mp4 "VIDEO - Importing PDF files and saving as EPS.")


**Microsoft Powerpoint (Proprietary)**
Create presentations and / or images and logos.

**PLUGIN:** PPsplit - Very good Power Point plugin that converts the animations in a presentation to a multipage PDF document. Useful for situations where you don't have powerpoint available to perform a presentation. http://www.maxonthenet.altervista.org/ppsplit.php


**Gnuplot for 3D graphics (Open source)**

**Matplot (Open source)***

**Virtualbox Windows Virtual Machine - (Free)**
Visual virtual machines manager. Useful to perform experiments or to test multiple operating systems. https://www.virtualbox.org