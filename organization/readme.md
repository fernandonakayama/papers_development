**Tools for organization**
- Joplin - Note taking  (Free) (Smartphone friendly)
Open-source application similar to EverNote. You can sync the desktop and smartphone versions using GDrive or Dropbox. https://joplinapp.org

- Master PDF (Freemium)
PDF editor with good editing and locking/unlocking tools. https://code-industry.net/get-masterpdfeditor/

- Mindmaster - Mindmaps (Freemium)
Tool to create and export mindmaps. https://www.edrawsoft.com/mindmaster/

- Texmaker - If overleaf is down (Open source)
Offline Latex editor. https://www.xm1math.net/texmaker/

- Gitkraken - GIT GUI (Freemium)
Visual Git manager. https://www.gitkraken.com