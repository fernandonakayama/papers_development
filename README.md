**I created this repository to compile the tools I am using to develop my scientific papers and relevant documents during my PhD.**

I split the tools into: (Itens marked with * have specific instructions)

[**Tools for generating graphics / images**](https://gitlab.com/fernandonakayama/papers_development/-/tree/master/images_graphics "Tools for generating graphics / images")
- Gimp - Image editing tool (Open source)
- Inkscape - Vectorial graphics tool (Open source)*
- Microsoft Powerpoint (Proprietary)*
- Gnuplot for 3D graphics (Open source)
- Matplot (Open source)*
- Virtualbox Windows Virtual Machine - (Free)

[**Multimedia tools**](https://gitlab.com/fernandonakayama/papers_development/-/tree/master/multimedia "**Multimedia tools**")
- StreamYard - (Freeware) - Stream content to Youtube*
- Kazam (Linux) - Screen capture tool that captures audio*
- Openshot (Linux) - Video and audio editing*

[**Tools for organization**](https://gitlab.com/fernandonakayama/papers_development/-/tree/master/organization "Tools for organization")
- Joplin - Note taking  (Free) (Smartphone friendly)
- Master PDF (Freemium)
- Mindmaster - Mindmaps (Freemium)
- Texmaker - If overleaf is down (Open source)
- Gitkraken - GIT GUI (Freemium)


[**Programming tools and scripts**](https://gitlab.com/fernandonakayama/papers_development/-/tree/master/programming "Programming tools and scripts")
- Anaconda navigator
- Spyder (Python)
- Jetbrains Goland (GO)
- Linux console

**Relevant sites and online tools**
- Google tools - https://docs.google.com
- Overleaf - Online latex editor - https://www.overleaf.com
- Tables Generator - Online tables generator for Latex/Overleaf - https://www.tablesgenerator.com
- ProWritingAid - Grammar Checker - https://prowritingaid.com
- Editor.MD - Online Markdown editor - https://pandao.github.io/editor.md/en.html
- PDF Diff - Compare two PDF files - https://www.diffchecker.com/pdf-diff
- Time zone converter (To manage international online appointments) - https://www.worldtimebuddy.com/utc-to-brazil-brasilia


[**Statistics tools**](https://gitlab.com/fernandonakayama/papers_development/-/tree/master/statistics "Statistical tools")
- R (Cran)
- JASP - Basic statistics software (Freemium)*

**My setup**

I'm currently using Kubuntu 20.04 as O.S. and a Lenovo Ideapad 300 laptop (i5 8th Gen - 4 cores 8 threads with 8GB RAM) All applications run flawlessly with this configuration.

I use windows in a virtual machine with windows 7 and 3GB or RAM. This allows me to switch between O.S.s without constantly rebooting my machine.

Whenever possible, a complete description of each tool and its context of use will be made. If the tool is visual, I will insert prints and usage tips.
