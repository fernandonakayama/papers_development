**Programming tools and scripts**
- Anaconda navigator
Visual manager for Conda. The most relevant use for this is dealing with mixed environments (Python 2.x and 3.x for instance). Since it allows creating multiple environments you can mod each environment to suit your needs. Very useful for multiple projects. https://www.anaconda.com/products/individual

- Spyder (Python)
Fast and simple IDE  that suports multiple programming languages (I use it with Python though) https://www.spyder-ide.org

- Jetbrains Goland (GO)
IDE for GO programming language. Jetbrains provides a free version of its IDEs if you have a institutional e-mail address. https://www.jetbrains.com/pt-br/go/

- Linux console
Well, its the linux console...